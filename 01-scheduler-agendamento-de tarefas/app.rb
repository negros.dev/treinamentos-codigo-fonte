require 'rufus-scheduler'


scheduler = Rufus::Scheduler.new


scheduler.every '5s' do
  File.write("arquivo.txt", "Tarefa sendo executada em #{Time.now}\n", mode: "a")
end

scheduler.every '6s' do
  puts File.read("arquivo.txt")
end


scheduler.join