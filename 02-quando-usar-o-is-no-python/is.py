a = 256
b = 256
print(a is b) # True
print(id(a))  # 139642367052176
print(id(b))  # 139642367052176


a = 257
b = 257
print(a is b) # False
print(id(a))  # 139642365408048
print(id(b))  # 139642365407824
